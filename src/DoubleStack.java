

import java.util.LinkedList;


public class DoubleStack {

   public static void main(String[] argum) {
      // TODO!!! Your tests here!
   }

   private LinkedList<Double> magasin = new LinkedList();

   DoubleStack() {
      magasin = new LinkedList();


      // TODO!!! Your constructor here!
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack koopia = new DoubleStack();
      if (!stEmpty()) {
         for (int i = 0; i < magasin.size(); i++) {
            koopia.magasin.addLast(magasin.get(i));
         }
      }

      return koopia; // TODO!!! Your code here!
   }

   public boolean stEmpty() {

      return (magasin.size() == 0); // TODO!!! Your code here!
   }

   public void push(double a) {
      magasin.addLast(a);

      // TODO!!! Your code here!
   }

   public double pop() {
      if (stEmpty()) {
         //System.out.println("Stack is empty!!!!");
         throw new IndexOutOfBoundsException("Stack is empty!!!!");
      }
      return magasin.removeLast(); // TODO!!! Your code here!
   } // pop

   public void op(String s) {

      if (magasin.size()<2){
        // System.out.println("There is not enough numbers in stack! Cannot execute operations!" + magasin);
         throw new IndexOutOfBoundsException("There is not enough numbers in stack! Cannot execute operations!");
      }
      double op2 = pop();
      double op1 = pop();
      if (s.equals("+")) {push(op1 + op2);}
     else if (s.equals("-")){ push(op1 - op2);}
     else if (s.equals("*")){ push(op1 * op2);}
     else if (s.equals("/")) {push(op1 / op2);}
       else{
         //System.out.println("Invalid input:"+ s +" + - * / expected");
         throw new IllegalArgumentException("Invalid input:"+ s +" + - * / expected");}
      // TODO!!!

   }

   public double tos() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Stack is empty!!!!");
      }
      return magasin.getLast(); // TODO!!! Your code here!
   }

   @Override
   public boolean equals(Object o) {
      if (magasin.equals(((DoubleStack) o).magasin)) {
         return true; // TODO!!! Your code here!
      } else {
         return false;
      }
   }

   @Override
   public String toString() {
      if (stEmpty()) {
         return "Stack is empty!!!!"; // TODO!!! Your code here!
      } else {
         return magasin.toString();
      }
   }
   public static double interpret(String pol) {
      if (pol.isEmpty()) { // kui String on tyhi, siis ytleme sellest kasutajale
         //System.out.println("You have entered an Empty String: "+"'"+pol+"'");
         throw new IndexOutOfBoundsException("You have entered an Empty String: "+"'"+pol+"'");
      }
      DoubleStack tmp = new DoubleStack();
      String[] p = pol.split(("\\s")); //kirjutame String pol massiivi
      int countOp=0;
      int countDouble=0;
      for (int i = 0; i < p.length; i++) {//kontrollime iga sisendi
         if (p[i].equals("+") || p[i].equals("-") || p[i].equals("*") || p[i].equals("/")) { //kui sisendiks on operaator +-*/, siis teostame tehe
            tmp.op(p[i]);
            countOp++;
         }
         else {//kui sisend on midagi muud, siis kontrollime: kui see on double, siis paneme ta magasiini
            try{
            tmp.push(Double.parseDouble(p[i]));
               countDouble++;
            }
            catch (NumberFormatException ex ){ // kui sisend ei ole double, siis ytleme sellest kasutajale
               if (p[i].isEmpty() || p[i].equals(null)) {
                  System.out.println("Empty input" +"'"+p[i]+"'"+ "in String! Please check your String: "+"'"+pol+"'");

               }else{
               System.out.println("Oops, " + "'"+p[i]+"'" + " is not a Double or Operator (+-*/). Please check your String: "+"'" +pol+"'");
                  }
               }
         }
      }
      if(countDouble-countOp!=1){
         //System.out.println("Error in String: " + pol + "Too many doubles in String!" );
         throw new RuntimeException("Error in String:" + pol + "Too many doubles in String!" );
      }
      return tmp.tos(); // TODO!!! Your code here!
   }
}


